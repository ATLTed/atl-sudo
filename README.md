# atlSudo

Simple wrapper for node-sudo that uses async await (via promisify-child-process) and uses some basic defaults.

See https://github.com/calmh/node-sudo

And https://github.com/jcoreio/promisify-child-process

See also atlShell for non-sudo usage:
https://bitbucket.org/ATLTed/atl-shell/src/master/


## Install

```shell
npm install git+https://bitbucket.org/ATLTed/atl-sudo.git#master
```

## Example Usage

```javascript
const atlSudo = require('atl-sudo');

run();

async function run() {
  // Simple usage with String
  await atlSudo('mkdir -p /tmp/atlShell');

  await atlSudo('open /tmp/atlShell');

  // String or Array usage wish stdout or stederr:
  
  // Use closures since we're defining the const stdout more
  // that once (and it can't be named to something else);
  // It returns an object like {stdout:'', stderr: ''}
  {
    const { stdout, stderr } = await atlSudo('ls -l /tmp');
    console.log(stdout);
  }

  {
    const { stdout, stderr } = await atlSudo([ 'ls', '-l', '/tmp' ]);
    console.log(stdout);
  }
}
```
const atlSudo = require('./index.js');

run();

async function run() {
  // Simple usage with String
  await atlSudo('mkdir -p /tmp/atlShell');

  await atlSudo('open /tmp/atlShell');

  // String or Array usage wish stdout or stederr:
  
  // Use closures since we're defining the const stdout more
  // that once (and it can't be named to something else);
  // It returns an object like {stdout:'', stderr: ''}
  {
    const { stdout, stderr } = await atlSudo('ls -l /tmp');
    console.log(stdout);
  }

  {
    const { stdout, stderr } = await atlSudo([ 'ls', '-l', '/tmp' ]);
    console.log(stdout);
  }
}